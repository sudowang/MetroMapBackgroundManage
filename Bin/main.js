var update = require('./Update/update');

var website = require('./App/app');

update().then(function() {
    return website();
}).then(function(address) {
    var open = require('open');
    open(address, function(err) {
        if (err) throw err;
        console.log('The user closed the browser');
    });
});
