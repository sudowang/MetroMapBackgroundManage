var zipFolder = require('./../Update/zipFolder');
var ftpService = require('./../Update/ftpService');

zipFolder('./../App', './App.zip').then(function() {
    return ftpService.upload('./App.zip', './App.zip');
}).then(function() {
    return ftpService.upload('./../Update/config.json', './metroProjectsManage.json');
}).then(function() {
    console.log('Upload Success'.green);
});
