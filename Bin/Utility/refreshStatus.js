var request = require('request');
var url = require('./../App/config.json').apiUrl;
request({
    url: url + '/allprojects/'
}, function(error, response, body) {
    var prjList = JSON.parse(JSON.parse(body));
    prjList.forEach(function(item) {
        var idex = item.PrjId;
        refresh(idex, false); //true刷新本地redis缓存,false刷新远程redis缓存
    });
});


function refresh(prjId, local) {
    var host = local ? "http://localhost:3002" : "http://61.129.33.248:3002";
    request({
        url: host + "/projectstatus/" + prjId + "/true/true"
    }, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            request({
                url: host + "/projectstatus/" + prjId + "/false/true"
            }, function(error, response, body) {
                console.log("缓存%s完成!", prjId);
            });
        } else {

        }
    });
}
