var Client = require('ftp');
var q = require('q');
var fs = require('fs');
var util = require('util');
var colors = require('colors');
var path = require('path');

function ftpClientFac() {
    var c = new Client();
    c.connect({
        host: '61.129.33.248',
        user: "xiaohaha",
        password: "hahaxiao123"
    });
    return c;
}

function checkUpdate() {
    var defer = q.defer();
    var config = require('./config.json');
    var version = config.version;
    var c = ftpClientFac();
    console.log('开始检查更新.....');
    c.on('ready', function() {
        c.get('./metroProjectsManage.json', function(err, stream) {
            if (err) throw err;
            stream.pipe(fs.createWriteStream(path.join(__dirname, './config.json')));
            stream.on('end', function() {
                fs.readFile(path.join(__dirname, './config.json'), function(err, data) {
                    if (err) throw err;
                    config = JSON.parse(data.toString());
                    if (version !== config.version) {
                        console.log('程序有更新,准备下载...'.green);
                        defer.resolve(true);
                    } else {
                        console.log('没有更新...'.green);
                        defer.resolve(false);
                    }
                    c.end();
                });
            });

        });
    });


    return defer.promise;
};

function downLoadFile(src, des) {
    var defer = q.defer();
    var c = ftpClientFac();
    var numBytes = 0;
    c.on('ready', function() {
        c.size(src, function(err, size) {
            numBytes = size;
        });
        c.get(src, function(err, stream) {
            if (err) throw err;
            var bar = require('progress-bar').create(process.stdout);
            stream.pipe(fs.createWriteStream(des));
            var total = 0;
            stream.on('data', function(chunk) {
                total += chunk.length;
                bar.update(total / numBytes);
                // console.log('已下载:%d%'.yellow, Math.round());
            })
            stream.on('end', function() {
                c.end();
                defer.resolve();
            });

        });
    });
    return defer.promise;
}

function uploadLoadFile(src, des) {
    var defer = q.defer();
    var c = ftpClientFac();
    var numBytes = 0;
    c.on('ready', function() {
        c.put(src, des, function(err) {
            if (err) throw err;
            c.end();
            defer.resolve();
        });
    });
    return defer.promise;
}

module.exports = {
    checkUpdate: checkUpdate,
    download: downLoadFile,
    upload: uploadLoadFile
};
