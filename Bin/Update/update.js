var AdmZip = require('adm-zip');
var fs = require('fs');
var config;
var q = require('q');
var colors = require('colors');
var ftpService = require('./ftpService');



//1.下载
//2.删除
//3.解压
//4.删除压缩包
function deleteZipFile() {
    config = require('./config.json');
    var defer = q.defer();
    fs.exists(config.zipFileName, function(exists) {
        if (exists) {
            fs.unlink(config.zipFileName, function() {
                defer.resolve();
            })
        } else {
            defer.resolve();
        }
    });
    return defer.promise;
}



function downloadZip(src, dest) {
    var defer = q.defer();
    console.log('已经开始下载...');
    ftpService.download(src, dest).then(function() {
        defer.resolve();
        console.log('\r\n下载完成...'.green);
    });
    return defer.promise;
}

function decompressZip() {
    console.log('开始解压...');
    var defer = q.defer();
    setTimeout(function() {
        var zip = new AdmZip(config.zipFileName);
        zip.extractAllTo(config.targetFolder, /*overwrite*/ true);
        defer.resolve();
    }, 1000);
    return defer.promise;
}

function uploadModule() {
    var defer = q.defer();
    ftpService.checkUpdate().then(function(needUpdate) {
        if (needUpdate) {
            deleteZipFile().then(function() {
                return downloadZip(config.srcZipFileName, config.zipFileName);
            }).then(function() {
                return decompressZip();
            }).then(function() {
                console.log('解压完成...'.green);
                deleteZipFile();
                console.log('好');
                defer.resolve();
            });
        } else {
            defer.resolve();
        }
    });
    return defer.promise;
};
module.exports = uploadModule;