var q = require('q');
var fs = require('fs');
var archiver = require('archiver');
var path = require('path');


function zipFile(src, dest) {

    var defer = q.defer();
    var output = fs.createWriteStream('./App.zip');
    var archive = archiver('zip');

    output.on('close', function() {
        defer.resolve();
    });

    archive.on('error', function(err) {
        throw err;
    });

    archive.pipe(output);
    archive.bulk([{
        expand: true,
        src: [src + '/**'],
        dest: dest
    }]);
    fs.readFile('./../Update/config.json', function(err, data) {
        var config = JSON.parse(data.toString());
        config.version = config.version + 1;
        console.log(JSON.stringify(config));
        data = JSON.stringify(config);
        setTimeout(function() {
            fs.writeFile('./../Update/config.json', data, function(err) {
                if (err) throw err;
                archive.finalize();
            });
        }, 1000);

    });

    return defer.promise;
}

module.exports = zipFile;
