   (function($) {

       function calendarWidget(el, params) {
           el.empty();
           //remove related handlders and dom elements;
           var now = new Date();
           var thismonth = now.getMonth();
           var thisyear = now.getYear() + 1900;

           var opts = {
               month: thismonth,
               year: thisyear,
               validTimeList: [], //合法的时间列表,其他的时间列为disabled
               queryDataTimes: []
           };

           $.extend(opts, params);

           var queryDataTimes = opts.queryDataTimes;
           var monthNames = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
           var dayNames = ['日', '一', '二', '三', '四', '五', '六'];
           var month = i = parseInt(opts.month);
           var year = parseInt(opts.year);
           var nxtmonth, nxtyear = year,
               premonth, preyear = year;

           if (month === 11) {
               nxtmonth = 0;
               nxtyear = year + 1;

           } else {
               nxtmonth = month + 1;
           }
           if (month === 0) {
               premonth = 11;
               preyear = year - 1;
           } else {
               premonth = month - 1;
           }


           var validDays = [];
           (function() { //filter valid dates
               var timeList = opts.validTimeList.map(function(item) {
                   var splits = item.split('-'); //默认是这个yyyy-MM-dd格式,其他的暂时不支持
                   return {
                       year: parseInt(splits[0]),
                       month: parseInt(splits[1]),
                       day: parseInt(splits[2])
                   };
               });

               validDays = timeList.filter(function(item) {
                   return item.year === year && item.month === month + 1;
               }).map(function(item) {
                   return item.day;
               });
           })();



           var table = '<div id="calendar">';

           table += '<div id="calendar-top" class="inverted ui fluid three item menu"><a id="calendar-pre-icon" class="item"><i  class="left arrow icon"></i></a><span class="item " id="calendar-text">' + year + '-' + monthNames[month] + '</span><a  id="calendar-next-icon" class="item"><i  class="right arrow icon"></i></a></div>';

           //table += ('<div id="current-month" class="ui inverted centered">' + year + '-' + monthNames[month] + '</div>');
           table += ('<div class="ui seven column  grid" ' + 'id="calendar-month">');

           table += '<div  class="row" id="calendar-head">';



           for (d = 0; d < 7; d++) {
               table += '<div class="column"><span class="ui item">' + dayNames[d] + '</span>' + '</div>';
           }

           table += '</div>';

           var days = getDaysInMonth(month, year);
           var firstDayDate = new Date(year, month, 1);
           var lastDayDate = new Date(year, month, days);
           var firstDay = firstDayDate.getDay();
           var lastDay = lastDayDate.getDay();
           var prev_m = month == 0 ? 11 : month - 1;
           var prev_y = prev_m == 11 ? year - 1 : year;
           var prev_days = getDaysInMonth(prev_m, prev_y);
           firstDay = (firstDay == 0 && firstDayDate) ? 7 : firstDay;
           var i = 0;
           var startJ = 0,
               endJ = 35;
           table += '</div>';

           table += '<div id="calendar-day-pane" class="ui seven column  grid">'
           for (var j = startJ; j < endJ; j++) {
               if (j % 7 === 0) table += ('<div  class="row">');
               if ((j < firstDay)) {
                   table += ('<div class="column menu inverted ui"><a class="ui item disabled">' + (prev_days - firstDay + j + 1) + '</a></div>');
                   // table += ('<td class="disabled inverted"><span class="">' + (prev_days - firstDay + j + 1) + '</span></td>');
               } else if ((j >= firstDay + getDaysInMonth(month, year))) {
                   i = i + 1;
                   table += ('<div class="column menu inverted ui"><a class="ui item disabled">' + i + '</a></div>');
                   // table += ('<td class="disabled inverted"><span class="">' + i + '</span></td>');
               } else {
                   var curDay = j - firstDay + 1;
                   var curDayStr = curDay < 10 ? ('0' + curDay.toString()) : curDay.toString();
                   if (validDays.indexOf(curDay) >= 0) {
                       var dataDate = year + '-' + monthNames[month] + '-' + curDayStr;
                       var aClass = queryDataTimes.indexOf(dataDate) >= 0 ? "ui  red circular label" : "ui  teal circular label";
                       var aAttr = 'data-date="' + dataDate + '" ';
                       var spanAttr = ((j + 1) % 7 === 0) ? 'data-position="left center"' : '';
                       table += ('<div class="column valid-day"><a class="' + aClass + '" ' + aAttr + '><span ' + spanAttr + '>' + curDayStr + '</span></a></div>');
                   } else {
                       table += ('<div class="column menu inverted ui"><span class="ui item">' + curDay + '</span></div>');
                   }
               }
               if (j % 7 == 6) table += ('</div>');
           }

           table += ('</div></div>');

           el.html(table);

           el.find('.valid-day>.label').on('click', function(event) {



               //$(this).popup('hide all');

               var date = $(this).attr('data-date');
               var index = queryDataTimes.indexOf(date);
               if (index >= 0) {
                   $(this).removeClass('red').addClass('teal');
                   queryDataTimes.splice(index, 1);
               } else {
                   var el = $(this).removeClass('teal').addClass('red');
                   queryDataTimes.push(date);

               }
               if (queryDataTimes.length === 2) {
                   if (opts.onClick) {
                       $.extend(event, {
                           clickDates: queryDataTimes
                       });
                       setTimeout(function() {
                           queryDataTimes = [];
                           $('#calendar-day-pane').find('.red').removeClass('red').addClass('teal');
                           opts.onClick(event);
                       }, 300);
                   }
               } else if (queryDataTimes.length === 1) {
                   event.stopPropagation();
                   var pop = $(this).children().popup({
                       content: "再选择一个日期来完成查询",
                       on: 'focus'
                   }).popup('show');

                   $(document).one('click', (function(pop) {
                       return function(e) {
                           pop.popup('hide all');
                       };
                   })(pop));


               }
           });

           $('#calendar-pre-icon').on('click', function() {
               $.extend(opts, {
                   month: premonth,
                   year: preyear,
                   queryDataTimes: queryDataTimes
               });
               //$(el).popup('hide all');
               calendarWidget(el, opts);
           });
           $('#calendar-next-icon').on('click', function() {
               $.extend(opts, {
                   month: nxtmonth,
                   year: nxtyear,
                   queryDataTimes: queryDataTimes
               });
               //$(el).popup('hide all');
               calendarWidget(el, opts);
           });

       }

       function findIndexInQueryDatesArray(array, date) {
           var index = -1;
           for (var i = 0; i < array.length; i++) {
               if (array[i].date === date) {
                   index = i;
                   break;
               }
           }
           return index;
       }

       function getDaysInMonth(month, year) {
           var daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
           if ((month == 1) && (year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) {
               return 29;
           } else {
               return daysInMonth[month];
           }
       }


       // jQuery plugin initialisation
       $.fn.calendarWidget = function(params) {
           calendarWidget(this, params);
           return this;
       };

   })(jQuery);
