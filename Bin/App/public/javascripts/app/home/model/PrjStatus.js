define(['./../initial', './../service/showMsg'], function() {
    var app = angular.module('App.home');
    app.factory('PrjStatus', ['$rootScope', '$filter',
        function($rootScope, $filter) {

            var PrjStatus = function() {
                this.PrjId = $rootScope.curPrj ? $rootScope.curPrj.PrjId : -1;
                this.Title = "";
                this.Detail = "";
                this.StatusImgs = [];
                this.Date = $filter('date')(new Date(), 'yyyy-MM-dd');
                this.IsEditOld = false;//是否在修改旧工况
            };

            return PrjStatus;
        }
    ]);
});
