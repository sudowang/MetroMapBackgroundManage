define(['./../initial', './../service/showMsg', './PrjStatus'], function() {
    var app = angular.module('App.home');
    app.factory('Project', ['$q', '$http', 'showMsg', 'PrjStatus',
        function($q, $http, showMsg, PrjStatus) {
            var Project = function() {
                this.center = null;
                this.monitorTypes = [];
                this.bindProjectList = [];
            };
            Project.allMonitorTypes = [];
            Project.allAutoSurveyStations = [];
            Project.addProject = function(obj) {
                var defer = $q.defer();
                $http({
                    method: 'post',
                    url: '/addProject',
                    data: obj
                }).success(function(data) {
                    if (data) {
                        var prj = data;
                        showMsg('创建成功,项目ID为' + prj.PrjId, 'info');
                        defer.resolve(prj);
                    } else {
                        showMsg('创建失败!', 'alert');
                        defer.resolve(null);
                    }
                });
                return defer.promise;
            };
            Project.getAllAutoSurveyStations = function(prj) {
                var defer = $q.defer();
                if (Project.allAutoSurveyStations > 0) {
                    prj.bindAutoSurveyStations(Project.allAutoSurveyStations);
                    defer.resolve(Project.allAutoSurveyStations);
                } else {
                    $http({
                        method: 'get',
                        url: '/autoSurveyStations'
                    }).success(function(data) {
                        Project.allAutoSurveyStations = data;
                        prj.bindAutoSurveyStations(Project.allAutoSurveyStations);
                        defer.resolve(Project.allAutoSurveyStations);
                    });
                }
                return defer.promise;
            };
            Project.getAllMonitorTypes = function() {
                var defer = $q.defer();
                if (Project.allMonitorTypes > 0) {
                    defer.resolve(Project.allMonitorTypes);
                } else {
                    $http({
                        method: 'get',
                        url: '/monitorTypes'
                    }).success(function(data) {
                        Project.allMonitorTypes = data;
                        defer.resolve(Project.allMonitorTypes);
                    });
                }
                return defer.promise;
            };
            Project.prototype.bindAutoSurveyStations = function(list) {
                var prj = this;
                list.forEach(function(item) {
                    if (item.prjid === prj.PrjId) {
                        item.bind = true;
                    } else {
                        item.bind = false;
                    }
                });
            };
            //允许工程更新自己的属性
            Project.prototype.updateProperty = function(propertyName) {
                var prj = this;
                var defer = $q.defer();
                prj.property = propertyName;
                if (!propertyName) {
                    alert('参数不正确!');
                    defer.resolve();
                } else {
                    $http({
                        method: 'post',
                        url: '/extraprojectinfo',
                        data: prj
                    }).success(function(data) {
                        if (data.toString() === 'true') {
                            // showMsg('修改成功!', 'info');
                        } else {
                            showMsg('发生错误,修改失败!', 'alert');
                        }
                        defer.resolve();
                    });
                }
                return defer.promise;
            };
            Project.prototype.getMonitorPointsDictionary = function() {
                var prj = this;
                var defer = $q.defer();
                if (prj.monitorPointsDictionary) {
                    defer.resolve(prj.monitorPointsDictionary);
                } else {
                    $http({
                        method: 'get',
                        url: '/monitorPointsDictionary/' + prj.PrjId.toString()
                    }).success(function(data) {
                        prj.monitorPointsDictionary = data;
                        defer.resolve(prj.monitorPointsDictionary);
                    });
                }
                return defer.promise;
            };
            Project.prototype.updateMonitorPointsDictionary = function(dict) {
                var prj = this;
                var defer = $q.defer();
                prj.monitorPointsDictionary = dict;
                $http({
                    method: 'post',
                    url: '/monitorPointsDictionary',
                    data: {
                        prjId: prj.PrjId,
                        dict: dict
                    }
                }).success(function() {
                    showMsg('上传监测点成功!', 'info');
                    defer.resolve();
                });
                return defer.promise;
            };
            Project.prototype.updatePrjCenter = function() {
                var prj = this;
                var defer = $q.defer();
                if (!prj.center) {
                    showMsg('请先在地图上指定项目地址', 'alert');
                    defer.resolve(false);
                } else {
                    var center = prj.Remark = JSON.stringify(prj.center);
                    $http({
                        method: 'post',
                        url: '/updatePrjCenter/',
                        data: {
                            'center': center,
                            'prjId': prj.PrjId
                        }
                    }).success(function(data) {
                        prj.Remark = JSON.stringify(prj.center);
                        showMsg('项目地址指定成功!', 'info');
                        defer.resolve(true);
                    });
                }
                return defer.promise;
            };
            Project.prototype.updateMonitorTypes = function(types) {
                var prj = this;
                var defer = $q.defer();
                $http({
                    method: 'post',
                    url: '/monitorTypes/',
                    data: {
                        'prjId': prj.PrjId,
                        'monitorTypes': types,
                        'autoSurveyList': types.indexOf('自动化收敛') > -1 ? Project.allAutoSurveyStations : null
                    }
                }).success(function(data) {
                    showMsg('修改监测类型成功!', 'info');
                    defer.resolve(true);
                });
                return defer.promise;
            };
            Project.prototype.getMonitorTypes = function() {
                var prj = this;
                var defer = $q.defer();
                if (prj.monitorTypes.length > 0) {
                    defer.resolve(prj.monitorTypes);
                } else {
                    $http({
                        method: 'get',
                        url: '/monitorTypes/' + prj.PrjId.toString()
                    }).success(function(data) {
                        prj.monitorTypes = data;
                        defer.resolve(prj.monitorTypes);
                    });
                }
                return defer.promise;
            };

            Project.prototype.getPrjStatusList = function() {
                var prj = this;
                var defer = $q.defer();
                if (prj.prjStatusList) {
                    defer.resolve(prj.prjStatusList);
                } else {
                    $http({
                        method: 'get',
                        url: '/prjstatus/' + prj.PrjId.toString()
                    }).success(function(data) {
                        if (data.length > 0) {
                            data.forEach(function(item) {
                                var st = new PrjStatus();
                                item = angular.extend(st, item);
                            });
                        }
                        prj.prjStatusList = data;
                        defer.resolve(prj.prjStatusList);
                    });
                }
                return defer.promise;
            };
            Project.prototype.deletePrjStatus = function(dataList) {
                var prj = this;
                var defer = $q.defer();
                $http({
                    method: 'delete',
                    url: '/prjstatus/' + prj.PrjId.toString() + '/' + JSON.stringify(dataList)
                }).success(function() {
                    showMsg('成功删除工况!', 'info');
                    defer.resolve(dataList);
                });
                return defer.promise;
            };
            Project.prototype.uploadPrjStatus = function(status) {
                var prj = this;
                var defer = $q.defer();
                status.BindPrjIdList = prj.bindProjectList.length > 0 ? prj.bindProjectList.map(function(item) {
                    return item.PrjId;
                }) : [];
                $http({
                    method: 'post',
                    url: '/prjstatus/',
                    data: status
                }).success(function(data) {
                    if (data.toString() === 'true') {
                        showMsg('上传工况成功!', 'info');
                        // prj.prjStatusList.push(status);
                        defer.resolve(true);
                    } else {
                        showMsg('发生错误!', 'alert');
                        defer.resolve(false);
                    }
                });
                return defer.promise;
            };

            Project.prototype.changeStatus = function(onOff) {
                var prj = this;
                var defer = $q.defer();
                $http({
                    method: 'post',
                    url: '/onOffStatus/',
                    data: {
                        prjId: prj.PrjId,
                        onOff: onOff
                    }
                }).success(function(data) {
                    if (data.toString() === 'true') {
                        defer.resolve(true);
                    } else {
                        showMsg('发生错误!', 'alert');
                        defer.resolve(false);
                    }
                });
                return defer.promise;
            };

            Project.prototype.refreshStatusCache = function() {
                var prj = this;
                var defer = $q.defer();
                $http({
                    method: 'get',
                    url: '/refreshStatusCache/' + prj.PrjId.toString()
                }).success(function(data) {
                    if (data.toString() === 'true') {
                        showMsg('刷新工况成功!', 'info');
                        defer.resolve(true);
                    } else {
                        showMsg('缓存工况发生错误!', 'alert');
                        defer.resolve(false);
                    }
                });
                return defer.promise;
            };
            //工况工程绑定
            Project.prototype.addBindProject = function(prjName) {
                var prj = this;
                var defer = $q.defer();
                $http({
                    method: 'post',
                    url: '/bindProject',
                    data: {
                        prjId: prj.PrjId,
                        prjName: prjName
                    }
                }).success(function(data) {
                    if (data.toString() === 'true') {
                        showMsg('绑定成功!', 'info');
                        defer.resolve(true);
                    } else {
                        showMsg('绑定工程发生错误!', 'alert');
                        defer.resolve(false);
                    }
                });
                return defer.promise;
            };
            Project.prototype.getBindProject = function() {
                var prj = this;
                var defer = $q.defer();
                $http({
                    method: 'get',
                    url: '/bindProject/' + prj.PrjId.toString()
                }).success(function(data) {
                    if (angular.isArray(data)) {
                        prj.bindProjectList = data;
                        defer.resolve(data);
                    } else {
                        defer.resolve([]);
                    }
                });
                return defer.promise;
            };
            Project.prototype.deleteBindProject = function(target) {
                var prj = this;
                var defer = $q.defer();
                $http({
                    method: 'DELETE',
                    url: '/bindProject/' + prj.PrjId.toString() + '/' + target.PrjName
                }).success(function(data) {
                    if (data.toString() === 'true') {
                        var index = prj.bindProjectList.indexOf(target);
                        if (index >= 0) {
                            prj.bindProjectList.splice(index, 1);
                        }
                        showMsg('删除绑定成功!', 'info');
                        defer.resolve(true);
                    } else {
                        showMsg('删除绑定工程发生错误!', 'alert');
                        defer.resolve(false);
                    }
                });
                return defer.promise;
            };
            return Project;
        }
    ]);
});
