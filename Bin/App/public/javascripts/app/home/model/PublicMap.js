define(['./../initial', './../service/showMsg'], function() {
    var app = angular.module('App.home');
    app.factory('PublicMap', ['$rootScope', '$compile',
        function($rootScope, $compile) {
            var map = L.Map.extend({
                mapSource: '高德',
                prjMarkerLayer: null //存放prj marker的layer
            });

            map.prototype.loadPublicMap = function(source) {
                var map = this;
                source = source ? source : '高德';
                map.mapSource = source;
                var layers = [];
                if (source === '天地图') {
                    layers = [L.tileLayer('http://{s}.tianditu.com/DataServer?T=vec_w&x={x}&y={y}&l={z}', {
                        attribution: '&copy; <a href="http://www.chinaonmap.com/map/index.html">天地图</a>',
                        subdomains: ['t2', 't3', 't4', 't5', 't6'],
                        detectRetina: true,
                        reuseTiles: true
                    }), L.tileLayer('http://{s}.tianditu.com/DataServer?T=cva_w&x={x}&y={y}&l={z}', {
                        attribution: '&copy; <a href="http://www.chinaonmap.com/map/index.html">天地图</a>',
                        subdomains: ['t2', 't3', 't4', 't5', 't6'],
                        detectRetina: true,
                        reuseTiles: true
                    })];
                } else if (source === '高德') {
                    layers = [L.tileLayer('http://webrd01.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=7&x={x}&y={y}&z={z}', {
                        attribution: '&copy; <a href="http://www.amap.com/">高德地图</a>',
                        detectRetina: true,
                        reuseTiles: true
                    })];
                }
                layers.forEach(function(item) {
                    map.addLayer(item);
                });
                map.attributionControl.setPrefix('<a target="_blank" href="http://61.129.33.248:3002">上勘院</a>');
                map.prjMarkerLayer = new L.LayerGroup().addTo(map);

            };
            map.prototype.addMarker = function(center) {
                var map = this;
                if (map.prjMarker) {
                    map.removeLayer(map.prjMarker);
                }
                if (!center) return null;
                var marker = L.marker([center.lat, center.lng], {
                    icon: L.AwesomeMarkers.icon({
                        icon: 'hh',
                        prefix: 'i',
                        markerColor: 'red',
                        iconColor: '#fff',
                        extraClasses: '  icon home prj-marker '
                    })
                });
                marker.addTo(map);
                marker.bindPopup($rootScope.curPrj.Name, {
                    closeButton: false
                }).openPopup();
                map.prjMarker = marker;
                return marker;
            };
            map.prototype.addClickedPoint = function(center) {
                var map = this;
                var offset = [0, 0];
                if (map.mapSource === '天地图') {
                    offset = [0.00205972005050015, -0.00445783138300726];
                }
                map.addMarker(center);
                center.lat -= offset[0];
                center.lng -= offset[1];
                return center;
            };
            map.prototype.addPrjMarker = function(center) {
                var map = this;
                var offset = [0, 0];
                if (map.mapSource === '天地图') {
                    offset = [0.00205972005050015, -0.00445783138300726];
                }
                var offsetCenter = null;
                if (center) {
                    offsetCenter = {
                        lat: center.lat + offset[0],
                        lng: center.lng + offset[1]
                    };
                }
                map.addMarker(offsetCenter);
            };
            return map;
        }
    ]);
});
