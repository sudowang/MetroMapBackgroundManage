define(['./../initial', './../model/Project'], function() {
    var app = angular.module('App.home');
    app.directive('addOn', ['$rootScope', '$location', 'Project',
        function($rootScope, $location, Project) {
            return {
                restrict: 'A',
                link: function(scope, iElement, iAttrs) {
                    //防止用户刷新的时候丢失这些全局数据
                    if ($location.path() === '/') {
                        $rootScope.home = true;
                    } else {
                        $rootScope.home = false;
                    }
                    if (!$rootScope.getCurPrj) {
                        $rootScope.getCurPrj = function() {
                            var prj = new Project();
                            var obj = JSON.parse(window.localStorage.getItem('curPrj'));
                            $rootScope.curPrj = angular.extend(prj, obj);
                        };
                    }
                    if (!$rootScope.setCurPrj) {
                        $rootScope.setCurPrj = function() {
                            localStorage.setItem('curPrj', JSON.stringify($rootScope.curPrj));
                        };
                    }
                    if (!$rootScope.goTo) {
                        $rootScope.goTo = function(where) {
                            $rootScope.setCurPrj();
                            if (where === "/") {
                                $rootScope.home = true;
                                $location.path('/');
                            } else if (where === 'addProject') {
                                $rootScope.home = false;
                                $location.path('/addProject');
                            } else {
                                $rootScope.home = false;
                                $location.path('/' + where + "/" + $rootScope.curPrj.PrjId);
                            }
                        };
                    }
                }
            };
        }
    ])
});
