define(['./../initial'], function() {
    var app = angular.module('App.home');
    app.directive('imgAddOn', ['$rootScope', '$location',
        function($rootScope, $location) {
            return {
                restrict: 'A',
                scope: {
                    imgSrc: '=',
                    isOld: '='
                },
                link: function(scope, iElement, iAttrs) {

                    var unwatch = scope.$watch('imgSrc', function(newV, oldV) {
                        if(scope.isOld || !newV)return;
                        var img = new mOxie.Image();
                        img.onload = function() {

                            img.embed(iElement.parent()[0], {
                                width: 300,
                                height: 300,
                                crop: true
                            });
                        };

                        img.onembedded = function() {
                            this.destroy();
                        };

                        img.onerror = function() {
                            this.destroy();
                        };

                        img.load(scope.imgSrc.getSource());
                        unwatch();
                    }, true);

                }
            };
        }
    ])
});
