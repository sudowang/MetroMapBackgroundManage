define(['./../initial'], function() {
    return (function() {
        var app = angular.module('App.home');
        app.directive('tabs', ['$rootScope', function($rootScope) {
            return {
                restrict: 'E',
                transclude: true,
                scope: true,
                controller: ['$scope', '$element',
                    function($scope, $element) {

                        var panes = $scope.panes = [];

                        $scope.select = function(pane) {
                            angular.forEach(panes, function(pane) {
                                pane.selected = false;
                            });
                            pane.selected = true;
                        };


                        this.addPane = function(pane) {
                            if (panes.length === 0) $scope.select(pane);
                            panes.push(pane);
                        };
                    }
                ],
                templateUrl: 'partials/tabs/tabs.html',
                replace: true
            };
        }]);


        app.directive('pane', ['$filter', '$window', '$rootScope',
            function($filter, $window, $rootScope) {
                return {
                    require: '^tabs',
                    restrict: 'E',
                    transclude: true,
                    scope: {
                        header: '@'
                    },
                    link: function(scope, element, attrs, tabsCtrl) {
                        scope.selected = false;
                        tabsCtrl.addPane(scope);
                    },
                    templateUrl: 'partials/tabs/pane.html',
                    replace: true
                };
            }
        ]);

        return true;
    })();
});
