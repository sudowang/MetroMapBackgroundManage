define(['./../initial', './../model/Project'], function() {
    var app = angular.module('App.home');
    app.factory('getAllProjects', ['$q', '$http', 'Project', '$rootScope',
        function($q, $http, Project, $rootScope) {
            var obj = {
                prjs: null,
                get: function(arguments) {
                    var defer = $q.defer();
                    if (!obj.prjs) {
                        $http({
                            url: '/allprojects',
                            method: 'get'
                        }).success(function(prjs) {
                            if (angular.isArray(prjs)) {
                                prjs.forEach(function(prj, index) {
                                    var prjItem = new Project();
                                    prjs[index] = angular.extend(prjItem, prj);
                                });
                            }
                            obj.prjs = prjs;
                            $rootScope.setCurPrj();
                            defer.resolve(prjs);
                        });
                    } else {
                        defer.resolve(obj.prjs);
                    }
                    return defer.promise;
                }
            };
            return obj;
        }
    ]);
});
