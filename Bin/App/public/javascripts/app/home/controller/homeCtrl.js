define(['./../service/getAllProjects', './../service/showMsg', './../model/Project'], function() {
    var app = angular.module('App.home');
    app.controller('homeCtrl', ['$scope', 'getAllProjects', '$rootScope', '$location', '$http', 'showMsg', 'Project', '$filter', "$timeout",
        function($scope, getAllProjects, $rootScope, $location, $http, showMsg, Project, $filter, $timeout) {


            var lineColors = ['#FB1806', '#36B854', '#FFD823', '#720BFD', '#823094', '#CF047A', '#F3560F', '#008CC1', '#91C5DB', '#C7AFD3', '#C12F2F', '#007A61', '#EC91CC'];

            getAllProjects.get().then(function(prjs) {
                // $scope.originprojects = angular.copy(prjs, $scope.originprojects);
                $rootScope.allProjectList = $scope.allprojects = prjs;
                var l = -1;
                var lines = prjs.filter(function(prj) {

                    if (prj.LineId !== l) {
                        l = prj.LineId;
                        return true;
                    } else return false;
                });

                $scope.lines = lines.map(function(item) {
                    return {
                        color: item.LineId - 1 >= 0 ? lineColors[item.LineId - 1] : 1,
                        line: item.LineId,
                        title: item.LineId + '号线',
                        active: false
                    };
                });
                $scope.lines.splice(0, 0, {
                    color: "#fff",
                    title: '全部线路',
                    line: -1,
                    active: true
                });
                //filter 所有项目
                $scope.status = [{
                    title: "所有项目",
                    status: undefined,
                    active: false
                }, {
                    title: "在地图上显示",
                    status: 1,
                    active: true
                }, {
                    title: "不在地图上显示",
                    status: 0,
                    active: false
                }];

                //初始化dropdown button
                $timeout(function() {
                    $('.ui.dropdown').dropdown();
                }, 1000);
            });
            $scope.searchLine = -1;
            $scope.search = undefined;
            $scope.searchStatus = 1;




            $scope.prjFilter = function(prj) {
                if (!$scope.search && $scope.searchLine === -1) return true;
                if ($scope.searchLine === -1 && $scope.search) {
                    return prj.Name.indexOf($scope.search) >= 0;
                } else if ($scope.searchLine >= 1 && !$scope.search) {
                    return prj.LineId === $scope.searchLine;
                } else if ($scope.searchLine >= 1 && $scope.search) {
                    return prj.LineId === $scope.searchLine && prj.Name.indexOf($scope.search) >= 0;
                }
            };
            $scope.setSearchLine = function(l) {
                $scope.searchLine = l.line;

                $scope.lines.forEach(function(item) {
                    item.active = false;
                });
                l.active = true;
            };
            $scope.setSearchStatus = function(s) {
                $scope.searchStatus = s.status;
                $scope.status.forEach(function(item) {
                    item.active = false;
                });
                s.active = true;
            };

            $scope.changeStatus = function(prj, onOff) {
                prj.changeStatus(onOff ? 1 : 0).then(function(result) {
                    if (result) {
                        prj.Status = onOff ? 1 : 0;
                    }
                });
            };

            $scope.changeProperty = function(prj, property) {
                prj.updateProperty(property);
            };

            $scope.selectPrj = function(prj) {
                $rootScope.curPrj = prj;
                $rootScope.setCurPrj();
                $rootScope.goTo('mapLocation');
            };

            $scope.refreshCache = function(typeCode) {
                $http({
                    method: 'get',
                    url: '/refreshCache/' + typeCode
                }).success(function(data) {
                    if (data.toString() === 'true') {
                        showMsg('刷新缓存成功!', 'info');
                    } else {
                        showMsg('发生错误!', 'alert');
                    }
                });
            };

            //添加一个新的项目
            $scope.lineNameList = (function() {
                var arr = [];
                for (var i = 1; i <= 13; i++) {
                    arr.push(i.toString() + '号线');
                }
                return arr;
            })();
            $scope.cityNameList = (function() {
                return ['上海', '南京', '杭州', '其他'];
            })();
            //             CriticalSectionMonitor
            // MetroMonitor
            // 
            $scope.projectTypeList = (function() {
                return [{
                    name: '地铁监护',
                    value: 'MetroMonitor'
                }, {
                    name: '公路隧道',
                    value: 'RoadTunnellMonitor'
                }, {
                    name: '重点段监护',
                    value: 'CriticalSectionMonitor'
                }];
            })();
            $scope.corporation = "";
            $scope.department = "";
            $scope.constructorList = (function() {
                return [{
                    name: '上勘院',
                    value: '上勘院',
                    department: ['一所', '二所', '三所']
                }];
            })();
            $scope.getDeparmentList = function() {
                var corp = $scope.constructorList.filter(function(item) {
                    return item.name === $scope.corporation;
                });
                if (corp.length > 0) corp = corp[0];
                else corp = null;
                return corp ? corp.department : [];
            };



            $scope.addProject = function() {
                if ($scope.prjName && $scope.lineName && $scope.cityName && $scope.projectType && $scope.corporation && $scope.department) {
                    Project.addProject({
                        prjName: $scope.prjName,
                        lineName: $scope.lineName,
                        cityName: $scope.cityName,
                        projectType: $scope.projectType,
                        corporation: $scope.corporation,
                        department: $scope.department
                    }).then(function(prj) {
                        if (!prj) return;
                        var index = 0;
                        for (var i = 0; i < $scope.allprojects.length; i++) {
                            if ($scope.allprojects[i].LineId < prj.LineId) {
                                index++;
                            } else {
                                break;
                            }
                        }
                        $scope.allprojects.splice(index, 0, prj);
                    });
                } else {
                    showMsg('请完整填写信息!', 'alert');
                }
            };
        }
    ]);
});
