define(['./../initial', './../service/showMsg', './../model/Project'], function() {
    var app = angular.module('App.home');
    app.controller('setRingNumbersCtrl', ['$scope', 'Project', '$rootScope', '$location', 'showMsg', '$http', "$timeout",
        function($scope, Project, $rootScope, $location, showMsg, $http, $timeout) {

            $rootScope.getCurPrj();
            $scope.hasUploaded = false;
            $http({
                method: 'get',
                url: '/ringnumberconfig/' + $rootScope.curPrj.PrjId.toString()
            }).success(function(suc) {
                if (suc.toString() === 'true') {
                    $scope.hasUploaded = true;
                }
            });


            var uploader = null;
            $timeout(function() {
                uploader = Qiniu.uploader({
                    runtimes: 'html5,flash,html4', //上传模式,依次退化
                    browse_button: 'excelUpload', //上传选择的点选按钮，**必需**
                    uptoken_url: '/token',
                    unique_names: true,
                    filters: {
                        mime_types: [{
                            title: "Excel files",
                            extensions: "xls,xlsx"
                        }]
                    },
                    domain: 'http://metroimages.qiniudn.com/',
                    container: 'container', //上传区域DOM ID，默认是browser_button的父元素，
                    max_file_size: '100mb', //最大文件体积限制
                    flash_swf_url: 'javascripts/bin/plupload/Moxie.swf', //引入flash,相对路径
                    max_retries: 3, //上传失败最大重试次数
                    multi_selection: false,
                    dragdrop: false, //开启可拖曳上传
                    chunk_size: '4mb', //分块上传时，每片的体积
                    auto_start: true, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
                    init: {
                        'FilesAdded': function(up, files) {
                            if (files.length > 1) {
                                alert('只允许上传一个文件!')
                                return;
                            } else {

                            }
                        },
                        'FileUploaded': function(up, file, info) {
                            var domain = up.getOption('domain');
                            info = JSON.parse(info);
                            var uploadItem = {
                                prjId: $rootScope.curPrj.PrjId,
                                fileName: info.key,
                                url: domain + info.key
                            };
                            $http({
                                method: 'post',
                                url: '/ringnumberconfig/',
                                data: uploadItem
                            }).success(function(data) {
                                if (data.toString() === 'true') {
                                    showMsg('上传里程与环号成功!', 'info');
                                    $scope.hasUploaded = true;
                                } else {
                                    showMsg('发生错误!', 'alert');
                                }
                            });
                        },
                        'Error': function(up, err, errTip) {
                            alert('上传出错,刷新页面再试!');
                        }
                    }
                });
            }, 400);

            $scope.confirm = function() {
                $rootScope.curPrj.updateMonitorTypes($scope.curTypes).then(function() {
                    $rootScope.setCurPrj();
                });
            };

        }
    ]);
});
