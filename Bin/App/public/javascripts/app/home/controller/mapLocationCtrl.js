define(['./../initial', './../model/PublicMap', './../service/showMsg', './../model/Project'], function() {
    var app = angular.module('App.home');
    app.controller('mapLocationCtrl', ['$scope', '$rootScope', 'PublicMap', 'showMsg', 'Project', '$location',
        function($scope, $rootScope, PublicMap, showMsg, Project, $location) {

            var originCenter = null;

            var mapHeight = window.document.documentElement.clientHeight - 180;
            $('#map').height(mapHeight + 'px');

            $rootScope.getCurPrj();

            if ($rootScope.curPrj.Remark) {
                originCenter = $rootScope.curPrj.center = JSON.parse($rootScope.curPrj.Remark);
            } else {
                showMsg("点击地图来指定位置", 'note');
            }
            var map = new PublicMap('map', {
                center: $rootScope.curPrj.center || [31.1881336, 121.4737701], //上海坐标,默认为地图中心
                zoom: $rootScope.curPrj.center ? 16 : 13,
                maxZoom: 18,
                minZoom: 8,
                zoomControl: false,
                doubleClickZoom: false
            });
            map.loadPublicMap('天地图');
            if ($rootScope.curPrj.center) {
                map.addPrjMarker($rootScope.curPrj.center);
            }
            map.on('click', function(event) {
                $rootScope.curPrj.center = map.addClickedPoint(event.latlng);
            });
            $scope.reset = function() {
                $rootScope.curPrj.center = originCenter;
                map.addPrjMarker($rootScope.curPrj.center);
            };
            $scope.confirm = function() {
                $rootScope.curPrj.updatePrjCenter();
                $rootScope.setCurPrj();
            };

        }
    ]);
});
