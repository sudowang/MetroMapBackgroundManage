define(['./../initial', './../service/showMsg', './../model/Project', './../directive/imgAddOn', './../model/PrjStatus'], function() {
    var app = angular.module('App.home');
    app.controller('uploadPrjStatusCtrl', ['$scope', 'Project', '$rootScope', '$location', 'showMsg', 'PrjStatus', '$http', '$timeout',
        function($scope, Project, $rootScope, $location, showMsg, PrjStatus, $http, $timeout) {

            $rootScope.getCurPrj();

            $rootScope.curPrj.getPrjStatusList().then(function(list) {
                $scope.prjStatusList = list;
            });

            //上传工程概况
            $scope.uploadPrjSummary = function() {
                $rootScope.curPrj.PrjSummary = $rootScope.curPrj.PrjSummary.replace(/\n/g, '\r\n');
                $rootScope.curPrj.updateProperty('PrjSummary').then(function() {
                    showMsg('上传工程概况成功');
                });
            };
            $scope.item = new PrjStatus();

            $scope.setCurItem = function(index) {
                $scope.item = angular.copy($scope.prjStatusList[index], $scope.item);
                $scope.item.IsEditOld = true;
                $scope.item.StatusImgs.forEach(function(item) {
                    item.isEditOld = true;
                });
            };

            $scope.changeDate = function(ele) {
                $scope.item.StatusImgs.forEach(function(item) {
                    item.Date = $scope.item.Date;
                });
            };

            $scope.removeImage = function(index) {
                $scope.item.StatusImgs.splice(index, 1);
            };

            var curIndex = -1;
            var imgPrefix = "1200w_900h_50q_";
            $scope.initUploader = function() {
                var uploader = Qiniu.uploader({
                    runtimes: 'html5,flash,html4', //上传模式,依次退化
                    browse_button: 'imgUpload', //上传选择的点选按钮，**必需**
                    uptoken_url: '/token',
                    unique_names: true,
                    domain: 'http://metroimages.qiniudn.com/',
                    container: 'container', //上传区域DOM ID，默认是browser_button的父元素，
                    max_file_size: '100mb', //最大文件体积限制
                    flash_swf_url: 'javascripts/bin/plupload/Moxie.swf', //引入flash,相对路径
                    max_retries: 3, //上传失败最大重试次数
                    dragdrop: false, //开启可拖曳上传
                    chunk_size: '4mb', //分块上传时，每片的体积
                    auto_start: false, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
                    init: {
                        'FilesAdded': function(up, files) {
                            // $scope.item.StatusImgs = [];
                            for (var i = 0; i < files.length; i++) {
                                $scope.$apply(function() {
                                    $scope.item.StatusImgs.push({
                                        PrjId: $rootScope.curPrj.PrjId,
                                        Date: $scope.item.Date.toString(),
                                        Url: "",
                                        Description: "",
                                        file: files[i],
                                        uploadFinish: false,
                                        key: "",
                                        originKey: "",
                                        percent: '0%',
                                        isEditOld: false
                                    });
                                });
                            }
                        },
                        'BeforeUpload': function(up, file) {
                            var index = findIndex(file);
                            var img = $('#img' + index);
                            img.addClass('upload-image');
                            img.css({
                                'display': 'block'
                            });
                        },
                        'UploadProgress': function(up, file) {
                            var index = findIndex(file);
                            var pp = file.percent + "%";
                            var img = $('#img' + index);

                            img.find('.bar').first().css({
                                'width': pp
                            });
                        },
                        'FileUploaded': function(up, file, info) {
                            var domain = up.getOption('domain');
                            var index = findIndex(file);
                            info = JSON.parse(info);
                            $scope.item.StatusImgs[index].originKey = info.key;
                            $scope.item.StatusImgs[index].key = imgPrefix + $scope.item.StatusImgs[index].originKey;
                            $scope.item.StatusImgs[index].Url = domain + $scope.item.StatusImgs[index].key;
                            //触发压缩程序
                            $http({
                                method: 'post',
                                url: '/imageCompress',
                                data: {
                                    key: $scope.item.StatusImgs[index].key,
                                    originKey: $scope.item.StatusImgs[index].originKey
                                }
                            }).success((function(ii) {
                                return function(data) {
                                    if (data.toString() === 'false') {
                                        console.log('压缩失败...');
                                    }
                                };
                            })(index));
                        },
                        'Error': function(up, err, errTip) {
                            alert('上传图片出错,刷新页面再试!');
                        },
                        'UploadComplete': function() {
                            //剔除没有URL的工况图片
                            $scope.item.StatusImgs = $scope.item.StatusImgs.filter(function(item) {
                                return item.Url.length > 0;
                            });

                            //队列文件处理完毕后,处理相关的事情
                            var alReady = $scope.prjStatusList.filter(function(item) {
                                return item.Date === $scope.item.Date;
                            });
                            if (alReady.length > 0) { //如果是更新已有的工况,先删除已有列表中的工况
                                $scope.alreadyIndex = $scope.prjStatusList.indexOf(alReady[0]);
                                $scope.prjStatusList.splice($scope.alreadyIndex, 1);
                                $rootScope.curPrj.prjStatusList = $scope.prjStatusList;
                            } else {
                                $scope.alreadyIndex = -1;
                            }

                            $rootScope.curPrj.uploadPrjStatus($scope.item).then(function(success) {
                                if (success) {
                                    if ($scope.alreadyIndex >= 0) {
                                        $rootScope.curPrj.prjStatusList.splice($scope.alreadyIndex, 0, $scope.item);
                                    } else {
                                        $rootScope.curPrj.prjStatusList.push($scope.item);
                                    }
                                    $scope.prjStatusList = $rootScope.curPrj.prjStatusList;
                                    $scope.add();
                                    $scope.item = new PrjStatus(); //重新初始化一下工况选项
                                    $rootScope.setCurPrj();
                                    console.log('Success!!');
                                }
                            });

                        }
                    }
                });
                return uploader;
            };

            function findIndex(file) {
                var index = -1;
                for (var i = 0; i < $scope.item.StatusImgs.length; i++) {
                    if ($scope.item.StatusImgs[i].file === file) {
                        index = i;
                    }
                }
                return index;
            }

            var plupload = null;
            $timeout(function() {
                plupload = $scope.initUploader();
            }, 400);


            $scope.confirm = function() {
                if (!$scope.item.Date || !$scope.item.Title || !$scope.item.Detail) {
                    alert('参数请填写完整!');
                } else {
                    plupload.start();
                }
            };
            //add new status
            $scope.add = function() {
                $scope.item = new PrjStatus();
            };

            $scope.delete = function() {
                if ($scope.prjStatusList.length === 0) return;
                var toBeDeleteIndexList = [];
                var selectedList = $scope.prjStatusList.filter(function(item, index, array) {
                    return item.Selected === true;
                });
                var selectedDateList = selectedList.map(function(item) {
                    return item.Date;
                });

                $rootScope.curPrj.deletePrjStatus(selectedDateList).then(function() {
                    selectedList.forEach(function(item) {
                        var index = $scope.prjStatusList.indexOf(item);
                        $scope.prjStatusList.splice(index, 1);
                    });
                    $rootScope.setCurPrj();
                });
            };
            $scope.canCache = true;
            $scope.refreshStatusCache = function() {
                if ($scope.canCache === false) return;
                showMsg('缓存请求已发送至后台,请等待通知.', 'note');
                $scope.canCache = false;
                $rootScope.curPrj.refreshStatusCache().then(function() {
                    $scope.canCache = true;
                });

            };
            //工程的相关同类工程绑定！！！
            //获取绑定列表
            $rootScope.curPrj.getBindProject().then(function(arr) {
                $rootScope.curPrj.bindPrjList = arr;
            });
            $scope.deleteBindPrj = function(prjName) {
                $rootScope.curPrj.deleteBindProject(prjName);
            };
            $scope.addBindProject = function(prjName) {
                if ($rootScope.allProjectList) {
                    var prjs = $rootScope.allProjectList.filter(function(prj) {
                        return prj.Name === prjName;
                    });
                    if (prjs.length !== 1) {
                        alert('输入的工程名称不正确！');
                        return;
                    } else {
                        $rootScope.curPrj.addBindProject(prjName).then(function() {
                            return $rootScope.curPrj.getBindProject();
                        }).then(function(arr) {
                            $rootScope.curPrj.bindPrjList = arr;
                        });
                    }
                }
            };


        }
    ]);
});
