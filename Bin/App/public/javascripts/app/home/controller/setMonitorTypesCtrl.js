define(['./../initial', './../service/showMsg', './../model/Project'], function() {
    var app = angular.module('App.home');
    app.controller('setMonitorTypesCtrl', ['$scope', 'Project', '$rootScope', '$location', 'showMsg',
        function($scope, Project, $rootScope, $location, showMsg) {

            $rootScope.getCurPrj();

            Project.getAllMonitorTypes().then(function(allTypes) {
                $scope.allTypes = allTypes;
                return $rootScope.curPrj.getMonitorTypes();
            }).then(function(curTypes) {
                $scope.curTypes = curTypes;
            });

            $scope.toggle = function(type) {
                var index = $scope.curTypes.indexOf(type);
                if (index === -1) {
                    $scope.curTypes.push(type);
                } else {
                    $scope.curTypes.splice(index, 1);
                }
            };
            $scope.bindStation = function(st) {
                if (st.bind === false && st.prjid > -1) return;
                st.bind = !st.bind;
                if (st.bind) {
                    st.prjid = $rootScope.curPrj.PrjId;
                } else {
                    st.prjid = -1;
                }
            };
            $scope.showAutoSurvey = false;
            $scope.stations = [];
            $scope.$watch('curTypes', function(newValues, oldValues) {
                if (!newValues) return;
                if (newValues.indexOf('自动化收敛') > -1) {
                    if ($scope.stations.length === 0) {
                        Project.getAllAutoSurveyStations($rootScope.curPrj).then(function(stations) {
                            $scope.stations = stations;
                        });
                    }
                    $scope.showAutoSurvey = true;
                } else {
                    $scope.showAutoSurvey = false;
                }
            }, true);
            $scope.confirm = function() {
                $rootScope.curPrj.updateMonitorTypes($scope.curTypes).then(function() {
                    $rootScope.setCurPrj();
                });
            };

        }
    ]);
});
