define(['./../initial', './../service/showMsg', './../model/Project'], function() {
    var app = angular.module('App.home');
    app.controller('setMonitorPointsCtrl', ['$scope', 'Project', '$rootScope', '$location', 'showMsg', '$timeout', '$http',
        function($scope, Project, $rootScope, $location, showMsg, $timeout, $http) {

            $rootScope.getCurPrj();
            $scope.prefixTypes = ["SX", "XX", "SP", "XP"];
            $scope.locationTypes = ["上行线", "下行线", "出入口"];
            $scope.originDict = null;

            $rootScope.curPrj.getMonitorTypes().then(function(types) {
                $scope.monitorTypes = types;
                return $rootScope.curPrj.getMonitorPointsDictionary();
            }).then(function(dict) {
                $scope.originDict = dict;
                $scope.dict = angular.copy($scope.originDict);;
                $scope.dict.forEach(function(item) {
                    if ($scope.prefixTypes.indexOf(item.Prefix) < 0) {
                        $scope.prefixTypes.push(item.Prefix);
                    }

                    if ($scope.locationTypes.indexOf(item.Location) < 0) {
                        $scope.locationTypes.push(item.Location);
                    }

                });

            });

            $scope.reset = function() {
                $scope.dict = angular.copy($scope.originDict);
            };

            $scope.delete = function() {
                $scope.dict = $scope.dict.filter(function(item) {
                    return !item.Selected;
                });
            };

            $scope.add = function() {
                var item = {
                    Prefix: "",
                    Start: 1,
                    End: 1,
                    Location: "",
                    MonitorType: $scope.monitorTypes[0],
                    PrjId: $rootScope.curPrj.PrjId
                };
                $scope.dict.push(item);
            };

            $scope.confirm = function() {
                $rootScope.curPrj.updateMonitorPointsDictionary($scope.dict).then(function() {
                    $rootScope.setCurPrj();
                });
            };


            //利用Excel上传
            var uploader = null;
            $timeout(function() {
                uploader = Qiniu.uploader({
                    runtimes: 'html5,flash,html4', //上传模式,依次退化
                    browse_button: 'excelUpload', //上传选择的点选按钮，**必需**
                    uptoken_url: '/token',
                    unique_names: true,
                    filters: {
                        mime_types: [{
                            title: "Excel files",
                            extensions: "xls,xlsx"
                        }]
                    },
                    domain: 'http://metroimages.qiniudn.com/',
                    container: 'container', //上传区域DOM ID，默认是browser_button的父元素，
                    max_file_size: '100mb', //最大文件体积限制
                    flash_swf_url: 'javascripts/bin/plupload/Moxie.swf', //引入flash,相对路径
                    max_retries: 3, //上传失败最大重试次数
                    multi_selection: false,
                    dragdrop: false, //开启可拖曳上传
                    chunk_size: '4mb', //分块上传时，每片的体积
                    auto_start: true, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
                    init: {
                        'FilesAdded': function(up, files) {
                            if (files.length > 1) {
                                alert('只允许上传一个文件!')
                                return;
                            } else {

                            }
                        },
                        'FileUploaded': function(up, file, info) {
                            var domain = up.getOption('domain');
                            info = JSON.parse(info);
                            var uploadItem = {
                                prjId: $rootScope.curPrj.PrjId,
                                fileName: info.key,
                                url: domain + info.key
                            };
                            $http({
                                method: 'post',
                                url: '/monitorpointsconfig/',
                                data: uploadItem
                            }).success(function(data) {
                                if (data.toString() === 'true') {
                                    showMsg('上传监测点配置成功!', 'info');
                                    $rootScope.curPrj.monitorPointsDictionary = null;
                                    $rootScope.curPrj.getMonitorPointsDictionary().then(function(dict) {
                                        $scope.originDict = dict;
                                        $scope.dict = angular.copy($scope.originDict);;
                                        $scope.dict.forEach(function(item) {
                                            if ($scope.prefixTypes.indexOf(item.Prefix) < 0) {
                                                $scope.prefixTypes.push(item.Prefix);
                                            }
                                            if ($scope.locationTypes.indexOf(item.Location) < 0) {
                                                $scope.locationTypes.push(item.Location);
                                            }

                                        });
                                    });
                                } else {
                                    showMsg('发生错误!', 'alert');
                                }
                            });
                        },
                        'Error': function(up, err, errTip) {
                            alert('上传出错,刷新页面再试!');
                        }
                    }
                });
            }, 400);



        }
    ]);
});
