 require.config({
     baseUrl: 'javascripts',
     paths: {
         jquery: 'bin/jquery/jquery',
         leaflet: 'bin/leaflet/leaflet',
         leafletdraw: 'bin/leaflet/leaflet.draw',
         // geoJsonConverter: 'bin/leaflet/jsonConverters',
         semantic: 'bin/semantic/semantic',
         highcharts: 'bin/highcharts/highcharts',
         jsonpack: 'bin/jsonpack/jsonpack',
         calendar: 'bin/jquery/jquery.calendar',
         leafletawe: 'bin/leaflet/leaflet.awesome-markers',
         plupload: 'bin/plupload/plupload.full.min',
         pluploadCn: 'bin/plupload/i18n/zh_CN',
         qiniu: 'bin/qiniu/qiniu'
     },
     shim: {
         'jquery': {
             'exports': 'jquery'
         },
         'leaflet': {
             'exports': 'leaflet'
         },
         'plupload': {
             'exports': 'plupload'
         },
         'plupload': ['jquery'],
         'pluploadCn': ['plupload'],
         'qiniu': ['plupload'],
         'leafletawe': ['leaflet'],
         'jsonpack': {
             'exports': 'jsonpack'
         },
         'calendar': ['jquery'],
         'semantic': ['jquery'],
         'highcharts': ['jquery'],
         'leafletdraw': ['leaflet']
     }
 });

 require(['app', 'jquery', 'leaflet', 'semantic', 'highcharts', 'jsonpack', 'calendar', 'leafletawe', 'plupload', 'pluploadCn', 'qiniu'], function() {
     'use strict';
     L.Icon.Default.imagePath = 'stylesheets/images';
     //手动启动app
     var htmlElement = document.getElementsByTagName("html")[0];
     var $html = angular.element(htmlElement);
     $html.attr("data-ng-app", "App");
     angular.element().ready(function() {
         angular.bootstrap($html, ["App"]);
     });
 });
