define([], function() {
    'use strict';
    var app = angular.module('App.routes', ['ngRoute']);
    app.config(['$routeProvider', '$locationProvider',
        function($routeProvider, $locationProvider) {
            $routeProvider.when('/', {
                templateUrl: 'partials/home.html',
                controller: 'homeCtrl'
            });
            $routeProvider.when('/setRingNumbers/:prjId', {
                templateUrl: 'partials/setRingNumbers.html',
                controller: 'setRingNumbersCtrl'
            });

            $routeProvider.when('/mapLocation/:prjId', {
                templateUrl: 'partials/mapLocation.html',
                controller: 'mapLocationCtrl'
            });
            $routeProvider.when('/uploadPrjStatus/:prjId', {
                templateUrl: 'partials/uploadPrjStatus.html',
                controller: 'uploadPrjStatusCtrl'
            });
            $routeProvider.when('/setMonitorTypes/:prjId', {
                templateUrl: 'partials/setMonitorTypes.html',
                controller: 'setMonitorTypesCtrl'
            });
            $routeProvider.when('/setMonitorPoints/:prjId', {
                templateUrl: 'partials/setMonitorPoints.html',
                controller: 'setMonitorPointsCtrl'
            });
            $routeProvider.otherwise({
                redirectTo: '/'
            });
            $locationProvider.html5Mode(true);
        }
    ]);

});
