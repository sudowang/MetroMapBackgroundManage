var qiniu = require('qiniu');
var config = require('./config.json');
qiniu.conf.ACCESS_KEY = config.ACCESS_KEY;
qiniu.conf.SECRET_KEY = config.SECRET_KEY;
var fs = require('fs');

qiniu.rsf.listPrefix(config.Bucket_Name, "o_", "", 1000, function(err, ret) {
    if (err) {
        console.log(JSON.stringify(err));
        return;
    }
    // fs.writeFile('files.json', JSON.stringify(ret));
    var files = ret.items;
    var client = new qiniu.rs.Client();
    files.forEach(function(item) {
        client.remove(config.Bucket_Name, item.key, function(err, res) {
            if (err) {
                console.log(JSON.stringify(err));
                return;
            } else {

            }
        });
    });
    console.log('Success! 一共移除了 %s 个原始图片.'.green, files.length);
});
