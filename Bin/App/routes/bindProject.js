module.exports = (function() {
    var express = require('express');
    var router = express.Router();
    var request = require('request');

    router.get('/:prjId', function(req, res) {
        var url = require('./../config.json').apiUrl;
        request({
            method: 'get',
            url: url + '/metroprojectgroup/',
            qs: req.params.prjId ? {
                prjId: req.params.prjId
            } : null
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                res.send(JSON.parse(body));
            } else {
                res.send('false');
            }
        });
    });
    router.delete('/:prjId/:prjName', function(req, res) {
        var url = require('./../config.json').apiUrl;
        request({
            method: 'delete',
            url: url + '/metroprojectgroup/',
            qs: {
                prjId: req.params.prjId,
                prjName: req.params.prjName
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                res.send('true');
            } else {
                res.send('false');
            }
        });
    });
    router.post('/', function(req, res) {
        var url = require('./../config.json').apiUrl;
        request({
            method: 'POST',
            url: url + '/metroprojectgroup/',
            json: JSON.stringify(req.body)
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                res.send(true);
            } else {
                console.log(JSON.stringify(error));
                res.send(false);
            }
        });
    });
    return router;
})();
