module.exports = (function() {
    var express = require('express');
    var router = express.Router();
    var request = require('request');

    router.get('/', function(req, res) {
        var url = require('./../config.json').apiUrl;
        request({
            url: url + '/autosurveystations/'
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                res.send(JSON.parse(body));
            } else {
                res.send('fail to load!');
            }
        });
    });
    return router;
})();
