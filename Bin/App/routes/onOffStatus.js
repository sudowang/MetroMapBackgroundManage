module.exports = (function() {
    var express = require('express');
    var router = express.Router();
    var request = require('request');

    router.post('/', function(req, res) {
        var url = require('./../config.json').apiUrl;
        request({
            method: 'POST',
            url: url + '/OnOffStatus/',
            json: JSON.stringify(req.body)
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                res.send(JSON.parse(body));
            } else {
                console.log(JSON.stringify(error));
                res.send(false);
            }
        });
    });
    return router;
})();
