var express = require('express');
var router = express.Router();
var qiniu = require('qiniu');
var config = require('./../config.json');
var colors=require('colors');
var request =require('request');
qiniu.conf.ACCESS_KEY = config.ACCESS_KEY;
qiniu.conf.SECRET_KEY = config.SECRET_KEY;

router.get('/', function(req, res) {
    res.send('123');
})
router.post('/', function(req, res) {
    var data = req.body;
    var newKey = data.key;
    var key = data.originKey;
    console.log('here comes compress!!'.red);
    (function(res) {
        compress(res, key, newKey);
    })(res);

});

function compress(res, key, newKey) {
    var bucket = config.Bucket_Name;
    var encodedEntryURI = qiniu.util.urlsafeBase64Encode(bucket + ":" + newKey);
    var signingStr = config.Bucket_Name + ".qiniudn.com/" + key + "?imageView2/3/w/1200/h/900/q/50|saveas/" + encodedEntryURI;
    var signed = qiniu.util.hmacSha1(new Buffer(signingStr), config.SECRET_KEY);
    var signedSafe1 = qiniu.util.base64ToUrlSafe(signed);
    var imgUrl = "http://" + signingStr + "/sign/" + config.ACCESS_KEY + ":" + signedSafe1;
    console.log("imgUrl is %s",imgUrl);
    request({
        url: imgUrl,
        method: 'GET',
    }, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log(JSON.stringify(body));
            console.log('压缩图片 %s 成功!'.green, key);
            res.send('true');
        } else {
            console.log(JSON.stringify(error));
            console.log('压缩图片 %s 失败!'.red, key);
            res.send('false');
        }
    });
};



module.exports = router;
