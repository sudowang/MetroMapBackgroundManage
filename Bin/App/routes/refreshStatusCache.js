module.exports = (function() {
    var express = require('express');
    var router = express.Router();
    var request = require('request');

    router.get('/:prjId', function(req, res) {
        var url = require('./../config.json').apiUrl;
        request({
            url: "http://61.129.33.248:3002/projectstatus/" + req.params.prjId + "/true/true"
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                request({
                    url: "http://61.129.33.248:3002/projectstatus/" + req.params.prjId + "/false/true"
                }, function(error, response, body) {
                    if (!error && response.statusCode == 200) {
                        res.send('true');
                    } else {
                        res.send('false');
                    }
                });
            } else {
                res.send('false');
            }
        });
    });
    return router;
})();
