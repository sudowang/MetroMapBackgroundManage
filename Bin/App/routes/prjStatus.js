module.exports = (function() {
    var express = require('express');
    var router = express.Router();
    var request = require('request');

    router.get('/:prjId?', function(req, res) {
        var url = require('./../config.json').apiUrl;
        request({
            url: url + '/prjstatus/',
            qs: req.params
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                res.send(JSON.parse(body));
            } else {
                res.send('fail to load!');
            }
        });
    });
    router.delete('/:prjId?/:date?', function(req, res) {
        var url = require('./../config.json').apiUrl;
        request({
            url: url + '/prjstatus/',
            qs: req.params,
            method: 'DELETE',
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                res.send(JSON.parse(body));
                refreshProjectCache();
            } else {
                res.send('fail to load!');
            }
        });
    });

    function refreshProjectCache() {
        request({
            url: "http://61.129.33.248:3002/projects/true"
        });
    }

    router.post('/', function(req, res) {
        var url = require('./../config.json').apiUrl;
        request({
            method: 'POST',
            url: url + '/prjstatus/',
            json: JSON.stringify(req.body)
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                refresh(req.body.PrjId, false);
                req.body.BindPrjIdList.forEach(function(prjId) {
                    refresh(prjId, false);
                });
                refreshProjectCache();
                res.send(true);
            } else {
                console.log(JSON.stringify(error));
                res.send(false);
            }
        });
    });

    function refresh(prjId, local) {
        var host = local ? "http://localhost:3002" : "http://61.129.33.248:3002";
        request({
            url: host + "/projectstatus/" + prjId + "/true/true"
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                request({
                    url: host + "/projectstatus/" + prjId + "/false/true"
                }, function(error, response, body) {
                    console.log("缓存%s完成!", prjId);
                });
            } else {

            }
        });
    }

    return router;
})();
