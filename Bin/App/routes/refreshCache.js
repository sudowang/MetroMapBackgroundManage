module.exports = (function() {
    var express = require('express');
    var router = express.Router();
    var request = require('request');

    router.get('/:typeCode', function(req, res) {
        var url = require('./../config.json').apiUrl;
        request({
            url: "http://61.129.33.248:3002/projects/true/" + req.params.typeCode
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                res.send('true');
            } else {
                res.send('false');
            }
        });
    });
    return router;
})();
