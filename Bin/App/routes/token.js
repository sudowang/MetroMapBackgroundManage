module.exports = (function() {
    var express = require('express');
    var router = express.Router();
    var config = require('./../config.json');
    var qiniu = require('qiniu');
    qiniu.conf.ACCESS_KEY = config.ACCESS_KEY;
    qiniu.conf.SECRET_KEY = config.SECRET_KEY;
    var uptoken = new qiniu.rs.PutPolicy(config.Bucket_Name);


    router.get('/', function(req, res) {
        var token = uptoken.token();
        res.header("Cache-Control", "max-age=0, private, must-revalidate");
        res.header("Pragma", "no-cache");
        res.header("Expires", 0);
        if (token) {
            res.json({
                uptoken: token
            })
        }
    });
    return router;
})();
