module.exports = (function() {
    var express = require('express');
    var router = express.Router();
    var request = require('request');

    router.get('/:prjId?', function(req, res) {
        var url = require('./../config.json').apiUrl;
        request({
            url: url + '/monitorPointsDictionary/',
            qs: req.params
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                res.send(JSON.parse(body));
            } else {
                res.send('fail to load!');
            }
        });
    });
    router.post('/', function(req, res) {
        var url = require('./../config.json').apiUrl;
        request({
            method: 'POST',
            url: url + '/monitorPointsDictionary/',
            json: JSON.stringify(req.body)
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                res.send(true);
            } else {
                console.log(JSON.stringify(error));
                res.send(false);
            }
        });
    });
    return router;
})();
