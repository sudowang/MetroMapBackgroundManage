module.exports = (function() {
    var express = require('express');
    var router = express.Router();
    var request = require('request');

    router.post('/', function(req, res) {
        var url = require('./../config.json').apiUrl;
        var prjId = req.body.prjId,
            centerJson = req.body.center;
        request({
            method: 'POST',
            url: url + '/updatePrjCenter/',
            json: JSON.stringify({
                'prjId': prjId,
                'center': centerJson
            })
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                res.send(true);
            } else {
                console.log(JSON.stringify(error));
                res.send(false);
            }
        });
    });
    return router;
})();
