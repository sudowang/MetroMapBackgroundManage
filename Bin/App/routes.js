module.exports = function(app) {
    // require('./replaceHtml.js');



    var projects = require('./routes/projects.js');
    var allprojects = require('./routes/allprojects.js');
    var updatePrjCenter = require('./routes/updatePrjCenter.js');
    var monitorTypes = require('./routes/monitorTypes.js');
    var autosurveystations = require('./routes/autosurveystations.js');
    var monitorPointsDictionary = require('./routes/monitorPointsDictionary.js');
    var prjStatus = require('./routes/prjStatus.js');
    var ringNumberConfig = require('./routes/ringNumberConfig.js');
    var imageCompress = require('./routes/imageCompress.js');
    var refreshCache = require('./routes/refreshCache.js');
    var refreshStatusCache = require('./routes/refreshStatusCache.js');
    var addProject = require('./routes/addProject.js');
    var bindProject = require('./routes/bindProject.js');
    var extraProjectInfo = require('./routes/extraProjectInfo.js');
    var monitorPointsConfig = require('./routes/monitorPointsConfig.js');

    app.use('/projects', projects);
    app.use('/allprojects', allprojects);
    app.use('/updatePrjCenter', updatePrjCenter);
    app.use('/monitorTypes', monitorTypes);
    app.use('/autosurveystations', autosurveystations);
    app.use('/monitorPointsDictionary', monitorPointsDictionary);
    app.use('/prjStatus', prjStatus);
    app.use('/ringNumberConfig', ringNumberConfig);
    app.use('/refreshCache', refreshCache);
    app.use('/onOffStatus', require('./routes/onOffStatus.js'));
    app.use('/refreshStatusCache', refreshStatusCache);
    app.use('/addProject', addProject);
    app.use('/extraProjectInfo', extraProjectInfo);
    app.use('/bindProject', bindProject);
    app.use('/monitorPointsConfig', monitorPointsConfig);
    
    //qiniu upload
    var token = require('./routes/token');
    app.use('/token', token);
    //qiniu 持久化压缩
    app.use('/imageCompress', imageCompress);
};
