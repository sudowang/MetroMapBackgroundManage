module.exports = function() {
    var q = require('q');
    var defer = q.defer();
    var express = require('express');
    var path = require('path');
    var favicon = require('static-favicon');
    var logger = require('morgan');
    var cookieParser = require('cookie-parser');
    var bodyParser = require('body-parser');
    var config = require('./config.json');


    var app = express();



    // view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'jade');


    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded());
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, 'public')));


    //use your own favicon
    app.use(favicon(path.join(__dirname, 'public/images/favicon.ico')));

    require('./routes.js')(app);
    /// error handlers

    // development error handler
    // will print stacktrace
    if (app.get('env') === 'development') {
        app.use(function(err, req, res, next) {
            res.status(err.status || 500);
            res.render('error', {
                message: err.message,
                error: err
            });
        });
    }
    app.use(function(req, res) {
        req.headers['if-none-match'] = 'no-match-for-this';
        res.sendfile(path.join(__dirname, '/public/index.html'));
    });

    // production error handler
    // no stacktraces leaked to user
    // app.use(function(err, req, res, next) {
    //     res.status(err.status || 500);
    //     res.render('error', {
    //         message: err.message,
    //         error: {}
    //     });
    // });

    require('colors');
    console.log('功能提示(2014-12-11):'.yellow);
    console.log('1.增加项目的时候需要填写部门和单位.'.green);
    var left = 3;
    var tick = function() {
        console.log('%d秒后启动网站...', left);
        left--;
        setTimeout(function() {
            if (left > 0) {
                tick();
            } else {
                var server = require('http').createServer(app).listen(config.port, function() {
                    var address = "http://localhost:" + config.port;
                    console.log('Your WebSite started at : ' + address.green);
                    defer.resolve(address);
                });
            }
            // body...
        }, 1000);
    };
    tick();
    return defer.promise;
};
